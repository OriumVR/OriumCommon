﻿using UnityEngine;
using System.Collections;
using Anima.Orium.Unity;

namespace Anima.Orium.Unity {

    [ExecuteInEditMode]
    public class OriumUnitySeat : MonoBehaviour {

        public int SeatIndex;
        public GameObject DefaultAvatar;

        private GameObject _avatar;

    	// Use this for initialization
        void Start () {
            if (!Application.isPlaying || OriumUnityClient.Instance == null)
            {
                return;
            }

            if (OriumUnityClient.Instance != null)
            {
                OriumUnityClient.Instance.RegisterSeat(this);
            }

            if (DefaultAvatar != null)
            {
                if (DefaultAvatar.transform.IsChildOf(transform)) 
                {
                    _avatar = DefaultAvatar;
                }
                else
                {
                    _avatar = GameObject.Instantiate(DefaultAvatar);
                    _avatar.transform.SetParent(transform, false);
                }
            }

            if (_avatar != null)
            {
                // ---------- THIS TOOK A WEEK TO DISCOVER ---------- //
                // THE AVATAR MUST BE ACTIVE WHEN SETTING THE CHANNEL //
                // OTHERWISE PLAYBACK WILL EXHIBIT STRANGE DISTORTION //
                _avatar.SetActive(true);

                OriumUnityTransformReceiver neckJoint = _avatar.GetComponentInChildren<OriumUnityTransformReceiver>();
                if (neckJoint != null)
                {
                    // TODO: Extract these string constants into a common file to prevent typos
                    neckJoint.Channel = "Orium.AudienceHead." + SeatIndex;
                }
                OriumUnityVoiceReceiver voiceReceiver = _avatar.GetComponentInChildren<OriumUnityVoiceReceiver>();
                if (voiceReceiver != null)
                {
                    voiceReceiver.Channel = "Orium.AudienceVoice." + SeatIndex;
                }

                _avatar.SetActive(false);
            }
        }

        public void SetSeatOccupation(bool isOccupied)
        {
            if (_avatar != null)
            {
                _avatar.SetActive(isOccupied);
            }
        }
    	
    	// Update is called once per frame
    	void Update () {
            if (!Application.isPlaying)
            {
                OriumPrefabPreview preview = GetComponent<OriumPrefabPreview>();
                if (preview)
                {
                    if (DefaultAvatar != null && !DefaultAvatar.activeInHierarchy)
                    {
                        preview.Prefab = DefaultAvatar;
                    }
                    else
                    {
                        preview.Prefab = null;
                    }
                }
            }
    	}
    }

}
