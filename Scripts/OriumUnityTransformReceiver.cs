﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Anima.Orium;
using Anima.Orium.Unity;

namespace Anima.Orium.Unity {

    public class OriumUnityTransformReceiver : MonoBehaviour {

        [SerializeField]
        private string _channel = null;
        public string Channel
        {
            get { return _channel; }
            set {
                if (value != _channel)
                {
                    if (OriumUnityClient.Instance != null)
                    {
                        OriumUnityClient.Instance.UnregisterCallback(typeof(OriumTransformSerializer), OnTransformPacket, _channel);
                    }

                    _channel = value;

                    if (OriumUnityClient.Instance != null)
                    {
                        OriumUnityClient.Instance.RegisterCallback(typeof(OriumTransformSerializer), OnTransformPacket, _channel);
                    }
                }
            }
        }
        void OnEnable()
        {
            if (OriumUnityClient.Instance != null)
            {
                OriumUnityClient.Instance.RegisterCallback(typeof(OriumTransformSerializer), OnTransformPacket, _channel);
            }
        }
        void OnDisable()
        {
            if (OriumUnityClient.Instance != null)
            {
                OriumUnityClient.Instance.UnregisterCallback(typeof(OriumTransformSerializer), OnTransformPacket, _channel);
            }
        }

        delegate void UpdateDelegate();
        private Queue<UpdateDelegate> _updates = new Queue<UpdateDelegate> ();
        private OriumUnityClient _client = null; 
        private Vector3 _newPosition = Vector3.zero;
        private Quaternion _newRotation = Quaternion.identity;
        private Vector3 _newScale = Vector3.zero;

        public float LerpConstant = 1.0f;
        public bool Position = true;
        public bool Rotation = true;
        public bool Scale = true;
        public Vector3 Unrotation = new Vector3(0.0f, 0.0f, 0.0f);

        // Update is called once per frame
        void Update () {
            if (_client == null && OriumUnityClient.Instance != null)
            {
                _client = OriumUnityClient.Instance;
            }

            lock (_updates)
            {
                while (_updates.Count > 0)
                {
                    _updates.Dequeue()();
                }
            }

            if (Position)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, _newPosition, LerpConstant);
            }
            if (Rotation)
            {
                transform.localRotation = Quaternion.Slerp(transform.localRotation, _newRotation, LerpConstant);
            }
            if (Scale)
            {
                transform.localScale = Vector3.Lerp(transform.localScale, _newScale, LerpConstant);
            }
        }

        void OnTransformPacket(long timestamp, ulong channel, object packet)
        {
            OriumTransformPacket transformPacket = (OriumTransformPacket)packet;
            lock (_updates)
            {
                _updates.Enqueue(delegate
                    {
                        _newPosition = new Vector3(
                            transformPacket.translation[0],
                            transformPacket.translation[1],
                            transformPacket.translation[2]);

                        _newRotation = Quaternion.Euler(Unrotation) * new Quaternion(
                            transformPacket.rotation[0],
                            transformPacket.rotation[1],
                            transformPacket.rotation[2],
                            transformPacket.rotation[3]) * Quaternion.Euler(-Unrotation);

                        _newScale = new Vector3(
                            transformPacket.scale[0],
                            transformPacket.scale[1],
                            transformPacket.scale[2]);
                    });
            }
        }
    }

}
