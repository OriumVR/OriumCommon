﻿using UnityEngine;
using System.Collections;

public class OriumUnitySoundSource : MonoBehaviour {

    public AudioClip AudioClip;
    public float Volume = 1.0f;
    public float Range = 1.0f;
    public bool PlayOnAwake = false;
    public bool Loop = false;

	// Use this for initialization
	void Start () {
        GameObject prefabInstance;

#if UNITY_ANDROID
        prefabInstance = Instantiate(Resources.Load<GameObject>("Prefabs/OriumSoundSourceCardboard"));
#else
        prefabInstance = Instantiate(Resources.Load<GameObject>("Prefabs/Internal/OriumSoundSource"));
#endif

        prefabInstance.transform.SetParent(transform, false);

#if UNITY_ANDROID
        CardboardAudioSource audioSource = prefabInstance.GetComponent<CardboardAudioSource>();
        audioSource.clip = AudioClip;
        audioSource.playOnAwake = PlayOnAwake;
        audioSource.loop = Loop;

        if(PlayOnAwake)
        {
            audioSource.Play();
        }
#else
        AudioSource audioSource = prefabInstance.GetComponent<AudioSource>();
        audioSource.clip = AudioClip;
        audioSource.playOnAwake = PlayOnAwake;
        audioSource.loop = Loop;
        audioSource.spatialBlend = 1.0f;
        audioSource.dopplerLevel = 0.0f;

        if(PlayOnAwake)
        {
            audioSource.Play();
        }
#endif
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawIcon(transform.position, "SpeakerIcon.png", true);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, Range);
    }
}
