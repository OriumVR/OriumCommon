﻿using UnityEngine;
using System.Collections;
using Anima.Orium.Unity;

public class OriumUnityVoicebox : MonoBehaviour {
    
    // Use this for initialization
    void Awake () {
        GameObject prefabInstance;

#if UNITY_ANDROID
        prefabInstance = Instantiate(Resources.Load<GameObject>("Prefabs/OriumVoiceboxCardboard"));
#else
        prefabInstance = Instantiate(Resources.Load<GameObject>("Prefabs/Internal/OriumVoicebox"));
#endif

        prefabInstance.transform.SetParent(transform, false);
    }
}
