﻿using UnityEngine;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class OriumPrefabPreview : MonoBehaviour
{
    public GameObject               Prefab;

    private static GameObject       _previewContainer;
    private static Scene            _scene;
    private static bool             _isPlaying = false;
    private static int              _previewCount = 1;

    [SerializeField]
    [HideInInspector]
    private int                     _instanceId = 0;

    void UpdatePreviewContainer()
    {
        if (gameObject.scene.isLoaded && _previewContainer == null)
        {
            GameObject[] rootObjects = gameObject.scene.GetRootGameObjects();
            foreach (GameObject rootObject in rootObjects)
            {
                if (rootObject.name == "Previews")
                {
                    _previewContainer = rootObject;
                }
            }
        }

        if (_previewContainer == null)
        {
            _previewContainer = new GameObject("Previews");
            _previewContainer.tag = "EditorOnly";
        }
    }

    void Instantiate()
    {
        UpdatePreviewContainer();

        GameObject _instance = Instantiate(Prefab);
        _instance.name = _instanceId.ToString();
        _instance.transform.parent = _previewContainer.transform;
        _instance.transform.position = transform.position;
        _instance.transform.rotation = transform.rotation;
        _instance.transform.localScale = Vector3.Scale(Prefab.transform.localScale,transform.lossyScale);
        _instance.tag = "EditorOnly";
    }

    void Destroy()
    {
        GameObject _instance = GetPrefabInstance();

        if (_instance != null)
        {
            if (Application.isPlaying)
            {
                Destroy(_instance);
            }
            else
            {
                DestroyImmediate(_instance);
            }
        }

        if(_previewContainer.transform.childCount == 0)
        {
            if (Application.isPlaying)
            {
                Destroy(_previewContainer);
            }
            else
            {
                DestroyImmediate(_previewContainer);
            }
        }
    }

    GameObject GetPrefabInstance()
    {
        UpdatePreviewContainer();

        Transform transform = _previewContainer.transform.FindChild(_instanceId.ToString());
        if(transform != null)
        {
            return transform.gameObject;
        }
        return null;
    }

    bool HasMoved()
    {
        bool moved = false;
        Transform currentTransform = transform;
        while (currentTransform != null)
        {
            if (currentTransform.hasChanged)
            {
                moved = true;
                break;
            }
            currentTransform = currentTransform.parent;
        }
        return moved;
    }

    void Update()
    {
        if (_scene != gameObject.scene ||
            _isPlaying != Application.isPlaying)
        {
            _previewCount = 0;
            _scene = gameObject.scene;
            _isPlaying = Application.isPlaying;

            UpdatePreviewContainer();

            if(Application.isPlaying)
            {
                Destroy(_previewContainer);
            }
            else
            {
                DestroyImmediate(_previewContainer);
            }
        }

        if(_instanceId == 0)
        {
            _instanceId = _previewCount;
            _previewCount++;
        }

        GameObject _instance = GetPrefabInstance();

        if ((!Application.isPlaying) && Prefab != null && _instance == null)
        {
            Instantiate();
        }

        if (Application.isPlaying || (Prefab == null && _instance != null))
        {
            Destroy();
            _instance = null;
        }

        if (_instance != null && HasMoved())
        {
            _instance.transform.position = transform.position;
            _instance.transform.rotation = transform.rotation;
            _instance.transform.localScale = Vector3.Scale(Prefab.transform.localScale,transform.lossyScale);
        }
	}

    void OnDisable()
    {
        Destroy();
    }

    void OnDestroy()
    {
        Destroy();
    }
}
