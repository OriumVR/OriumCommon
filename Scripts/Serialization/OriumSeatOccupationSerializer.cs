﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Anima.Orium;

namespace Anima.Orium
{
    class OriumSeatOccupationPacket
    {
        public int SeatIndex;
        public bool IsOccupied;
    }

    class OriumSeatOccupationSerializer : OriumBaseSerializer
    {
        public override string MessageId
        {
            get { return "Orium.SeatOccupation"; }
        }

        public override void Serialize(object packet, System.IO.BinaryWriter bw)
        {
            OriumSeatOccupationPacket seatPacket = (OriumSeatOccupationPacket)packet;
            bw.Write(seatPacket.SeatIndex);
            bw.Write(seatPacket.IsOccupied);
        }

        public override object Deserialize(System.IO.BinaryReader br)
        {
            return new OriumSeatOccupationPacket
            {
                SeatIndex = br.ReadInt32(),
                IsOccupied = br.ReadBoolean()
            };
        }
    }
}
