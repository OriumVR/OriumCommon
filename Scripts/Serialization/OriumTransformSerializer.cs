﻿using Anima.Orium;
using System.Collections;
using System.IO;

namespace Anima.Orium {

    public class OriumTransformPacket
    {
        public const int TranslationComponents = 3;
        public const int RotationComponents = 4;
        public const int ScaleComponents = 3;

        public float[] translation = new float[TranslationComponents];
        public float[] rotation = new float[RotationComponents];
        public float[] scale = new float[ScaleComponents];
    }

    public class OriumTransformSerializer : OriumBaseSerializer {

        public override string MessageId
        {
            get { return "TransformMsg"; }
        }

        public override double Priority
        {
            get { return 0.5; }
        }

        public override void Serialize(object packet, BinaryWriter bw)
        {
            OriumTransformPacket transformPacket = (OriumTransformPacket)packet;
            for (int i = 0; i < OriumTransformPacket.TranslationComponents; ++i)
            {
                bw.Write(transformPacket.translation[i]);
            }
            for (int i = 0; i < OriumTransformPacket.RotationComponents; ++i)
            {
                bw.Write(transformPacket.rotation[i]);
            }
            for (int i = 0; i < OriumTransformPacket.ScaleComponents; ++i)
            {
                bw.Write(transformPacket.scale[i]);
            }
        }

        public override object Deserialize(BinaryReader br)
        {
            OriumTransformPacket transformPacket = new OriumTransformPacket();
            for (int i = 0; i < OriumTransformPacket.TranslationComponents; ++i)
            {
                transformPacket.translation[i] = br.ReadSingle();
            }
            for (int i = 0; i < OriumTransformPacket.RotationComponents; ++i)
            {
                transformPacket.rotation[i] = br.ReadSingle();
            }
            for (int i = 0; i < OriumTransformPacket.ScaleComponents; ++i)
            {
                transformPacket.scale[i] = br.ReadSingle();
            }
            return transformPacket;
        }

    }

}
