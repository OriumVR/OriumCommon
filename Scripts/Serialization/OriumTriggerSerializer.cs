﻿using System.IO;
using Anima.Orium;

public class OriumTriggerSerializer : OriumBaseSerializer {

    public override string MessageId {
        get { return "Orium.Trigger"; }
    }

    public override double Priority {
        get { return 1.0; }
    }

    public override void Serialize(object packet, BinaryWriter bw)
    {
        bw.Write((ulong)packet);
    }

    public override object Deserialize(BinaryReader br)
    {
        return br.ReadUInt64();
    }
}
