﻿using System.Collections;
using System.IO;
using VoiceChat;

namespace Anima.Orium {

    public class OriumVoiceSerializer : OriumBaseSerializer {

        public override string MessageId {
            get { return "VoiceChatMsg"; }
        }

        public override double Priority
        {
            get { return 0.5; }
        }

        public override void Serialize(object packet, BinaryWriter bw)
        {
            VoiceChatPacket voiceChatPacket = (VoiceChatPacket)packet;

            bw.Write ((short)voiceChatPacket.Compression);
            bw.Write (voiceChatPacket.Length);
            bw.Write ((ushort)voiceChatPacket.Data.Length);
            bw.Write (voiceChatPacket.Data);
        }

        public override object Deserialize(BinaryReader br)
        {
            return new VoiceChatPacket {
                Compression = (VoiceChatCompression)br.ReadInt16(),
                Length = br.ReadInt32(),
                Data = br.ReadBytes(br.ReadUInt16())
            };
        }
    }

}
