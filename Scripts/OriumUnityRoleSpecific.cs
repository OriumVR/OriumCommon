﻿using UnityEngine;
using System.Collections;

namespace Anima.Orium.Unity {

    public class OriumUnityRoleSpecific : MonoBehaviour {

        public enum EnablePolicy
        {
            OnlyEnabledInThisRole,
            OnlyDisabledInThisRole
        }

        public OriumRole Role;
        public EnablePolicy Policy;

        OriumUnityClient _client = null;
    	
    	// Update is called once per frame
    	void Update () {
            if (_client == null)
            { 
                _client = OriumUnityClient.Instance;
                if (_client != null)
                {
                    bool roleMatches = Role == _client.Role;

                    if (Policy == EnablePolicy.OnlyEnabledInThisRole)
                    {
                        gameObject.SetActive(roleMatches);
                    }
                    else
                    {
                        gameObject.SetActive(!roleMatches);
                    }
                }
            }
    	}
    }

} 
