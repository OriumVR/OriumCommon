﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System;
using System.Reflection;
using System.Collections.Generic;

public class OriumSetLoader : MonoBehaviour {

    WWW _bundleLoad;
    AssetBundleRequest _assetsLoad;

    AssetBundle _assetBundle;
    Assembly _setBundleAssembly;

    public UnityEvent SceneLoaded;
    public UnityEvent SceneFailed;

    void Log(string message, bool isError = false)
    {
//        DebugConsole.Log(message, isError ? "error" : "normal");

        if (isError)
        {
            Debug.LogError(message);
        }
        else
        {
            Debug.Log(message);
        }
    }

    void ReloadSet()
    {
        List<GameObject> rootGameObjects = new List<GameObject>();
        SceneManager.GetActiveScene().GetRootGameObjects(rootGameObjects);
        foreach (GameObject rootObject in rootGameObjects)
        {
            if (rootObject.name == "SceneRoot" || rootObject.name == "SceneRoot (Clone)")
            {
                Destroy(rootObject);
            }
        }

        if (_assetBundle != null)
        {
            _assetBundle.Unload(true);
            _assetBundle = null;
        }

        string bundlePath = "file://" + Application.persistentDataPath + "/testbundle";

        Log("Searching For SetBundle In Path: " + bundlePath);

        _bundleLoad = new WWW(bundlePath);
    }

    void Start()
    {
        ReloadSet();
    }
	
	// Update is called once per frame
	void Update () {
        if (_bundleLoad != null && _bundleLoad.isDone)
        {
            WWW bundleLoad = _bundleLoad;
            _bundleLoad = null;

            if (!String.IsNullOrEmpty(bundleLoad.error))
            {
                Log(bundleLoad.error, true);
                return;
            }

            Log("Load Complete, Getting Bundle:");

            _assetBundle = bundleLoad.assetBundle;

            if (_assetBundle == null)
            {
                Log("Failed To Load Asset Bundle!", true);

                SceneFailed.Invoke();

                return;
            }

            Log("Bundle Acquired, Loading Scripts:");

            try
            {
            TextAsset scriptText = _assetBundle.LoadAsset<TextAsset>("SetBundleScripts");
                if (scriptText != null)
                {
                    try
                    {
                        _setBundleAssembly = Assembly.Load(scriptText.bytes);
                    }
                    catch (Exception e)
                    {
                        Log(e.ToString(), true);
                    }
                }
                else
                {
                    Log("Failed To Load SetBundleScripts!", true);
                }
            }
            catch (Exception ex)
            {
                Log(ex.ToString(), true);
            }


            Log("Scripts Loaded, Loading Assets:");

            _assetsLoad = bundleLoad.assetBundle.LoadAllAssetsAsync();
        }

        if (_assetsLoad != null && _assetsLoad.isDone)
        {
            _assetsLoad = null;

            Log("Assets Loaded, Instantiating Scene:");

            GameObject sceneRootPrefab = _assetBundle.LoadAsset<GameObject>("SceneRoot");
            if (sceneRootPrefab != null)
            {
                Instantiate(sceneRootPrefab);

                Log("Scene Instantiated, DONE!");

                SceneLoaded.Invoke();
            }
            else
            {
                SceneFailed.Invoke();
            }
        }
	}
}
