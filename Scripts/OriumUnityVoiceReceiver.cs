﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Anima.Orium;
using Anima.Orium.Unity;
using VoiceChat;

namespace Anima.Orium.Unity {

    public class OriumUnityVoiceReceiver : MonoBehaviour {

        [SerializeField]
        private string _channel = null;
        public string Channel
        {
            get { return _channel; }
            set {
                if (value != _channel)
                {
                    if (OriumUnityClient.Instance != null)
                    {
                        OriumUnityClient.Instance.UnregisterCallback(typeof(OriumVoiceSerializer), OnVoicePacket, _channel);
                    }

                    _channel = value;

                    if (OriumUnityClient.Instance != null)
                    {
                        OriumUnityClient.Instance.RegisterCallback(typeof(OriumVoiceSerializer), OnVoicePacket, _channel);
                    }
                }
            }
        }
        void OnEnable()
        {
            if (OriumUnityClient.Instance != null)
            {
                OriumUnityClient.Instance.RegisterCallback(typeof(OriumVoiceSerializer), OnVoicePacket, _channel);
            }
        }
        void OnDisable()
        {
            if (OriumUnityClient.Instance != null)
            {
                OriumUnityClient.Instance.UnregisterCallback(typeof(OriumVoiceSerializer), OnVoicePacket, _channel);
            }
        }

        private IVoiceChatPlayer _player;
        private Queue<VoiceChatPacket> _pendingPackets = new Queue<VoiceChatPacket>();
        public float MaxAcceptableLag = 2.0f;

    	// Use this for initialization
    	void Start () {

            object playerObj = null;

#if UNITY_ANDROID
            try
            {
                playerObj = gameObject.GetComponent(Type.GetType("VoiceChat.VoiceChatPlayerCardboard"));
            }
            catch (Exception) { }
#endif

            if (playerObj == null)
            {
                playerObj = gameObject.GetComponent(Type.GetType("VoiceChat.VoiceChatPlayer"));
                if (playerObj == null)
                {
                    return;
                    //                gameObject.AddComponent(Type.GetType("AudioSource"));
                    //                _player = (IVoiceChatPlayer)gameObject.AddComponent(Type.GetType("VoiceChat.VoiceChatPlayer"));
                }
            }

            _player = (IVoiceChatPlayer)playerObj;
        }
    	
        void OnVoicePacket(long timestamp, ulong channel, object packet)
        {
            if (OriumUnityClient.Instance.ServerTime - timestamp > TimeSpan.FromSeconds(MaxAcceptableLag).Ticks)
            {
                return;
            }

            _pendingPackets.Enqueue((VoiceChatPacket)packet);
        }

        void Update() {
            while (_player != null && _pendingPackets.Count > 0)
            {
                _player.OnNewSample(_pendingPackets.Dequeue());
            }
        }
    }

}
