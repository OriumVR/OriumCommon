﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using VoiceChat;
using WebSocketSharp;
using System.Diagnostics;
using Anima.Orium;

namespace Anima.Orium.Unity {

    public enum OriumRole
    {
        Audience,
        Broadcaster,
        Spectator
    }

    public class OriumUnitySendChannel 
    {
        private readonly OriumProtocol _protocol;
        private ulong _typeHash;
        private ulong _channelHash;

        internal OriumUnitySendChannel(OriumProtocol protocol, ulong typeHash, ulong channelHash)
        {
            _protocol = protocol;
            _typeHash = typeHash;
            _channelHash = channelHash;
        }

        public void Send(object packet)
        {
            if (!OriumUnityClient.Instance.IsConnected)
                return;

            _protocol.Send(_typeHash, packet, _channelHash);
        }
    }

    public class OriumUnityClient : MonoBehaviour
    {
        private static OriumUnityClient _instance;
        public static OriumUnityClient Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<OriumUnityClient>();
                }
                return _instance;
            }
        }

        public string WebSocketAddress = "ws://127.0.0.1:2525/";
        public bool IsConnected = false;
        public OriumRole Role = OriumRole.Audience;

        public float LagAverage = 0.0f;
        public float LagVelocity = 0.0f;
        public float DropRatio = 0.0f;
        public float OtherDropRatio = 0.0f;

        public event EventHandler Connected;
        public event EventHandler ConnectionError;
        public event EventHandler Disconnected;

        private readonly OriumRegistry _registry;
        private readonly OriumProtocol _protocol;

        private Dictionary<int,OriumUnitySeat> _seats = new Dictionary<int, OriumUnitySeat>();
        private Queue<Action> _updateActions = new Queue<Action>();
        private int _allocatedSeat = -1;

        private float _nextConnectAttemptTime = 0.0f;

        public long ServerTime
        {
            get { return _protocol.GetServerTime(); }
        }

        public OriumUnityClient()
        {
            _registry = new OriumRegistry();
            _protocol = new OriumProtocol(_registry, new OriumWSSTransportLayer());

            _protocol.RegisterCallback(typeof(OriumSeatAllocationSerializer), OnSeatAllocated);
            _protocol.RegisterCallback(typeof(OriumSeatOccupationSerializer), OnSeatOccupied);
            _protocol.RegisterFallback(OnMessageFallback);
        }

        public void RegisterSeat(OriumUnitySeat seat)
        {
            _seats.Add(seat.SeatIndex, seat); 
        }

        /// <summary>
        /// Registers a callback for the given message type.
        /// </summary>
        public void RegisterCallback(Type serializerType, OriumCallback callback, string channel = null)
        {
            ulong channelHash = 0;
            if (!string.IsNullOrEmpty(channel))
            {
                channelHash = OriumUtils.CalculateHash(channel);
            }

            _protocol.RegisterCallback(serializerType, callback, channelHash);
        }

        public void UnregisterCallback(Type serializerType, OriumCallback callback, string channel = null)
        {
            ulong channelHash = 0;
            if (!string.IsNullOrEmpty(channel))
            {
                channelHash = OriumUtils.CalculateHash(channel);
            }

            _protocol.UnregisterCallback(serializerType, callback, channelHash);
        }

        public OriumUnitySendChannel CreateSendChannel(Type serializerType, string channelId = null)
        {
            if (!_registry.HasSerializer(serializerType))
            {
                _registry.RegisterSerializer(serializerType);
            }

            ulong typeHash = _registry.GetSerializerHash(serializerType);
            ulong channelHash = 0;
            if (!channelId.IsNullOrEmpty())
            {
                channelHash = OriumUtils.CalculateHash(channelId);
            }

            return new OriumUnitySendChannel(_protocol, typeHash, channelHash);
        }

        public void Send(Type serializerType, object packet)
        {
            // DO NOT GET _socket.IsAlive - this is VERY EXPENSIVE!!!
            if (!IsConnected)
            {
                return;
            }

            _protocol.Send(serializerType, packet);
        }

        void Start () 
        {
            _protocol.Connected += OnSocketOpen;
            _protocol.ConnectionError += OnSocketError;
            _protocol.Disconnected += OnSocketClose;
        }

        void OnDestroy()
        {
            _protocol.Disconnect();
        }

        public void Connect()
        {
            _protocol.Connect(WebSocketAddress);
        }

        void Update()
        {
            if (_nextConnectAttemptTime > 0.0f)
            {
                _nextConnectAttemptTime -= Time.deltaTime;

                if (_nextConnectAttemptTime <= 0.0f)
                {
                    _nextConnectAttemptTime = float.MaxValue;

                    _protocol.Connect(WebSocketAddress);
                }
            }

            if (IsConnected)
            {
                _protocol.Update();

                LagAverage = (float)TimeSpan.FromTicks(_protocol._lagTimeAverage).TotalSeconds;
                LagVelocity = (float)TimeSpan.FromTicks(_protocol._lagTimeVelocity).TotalSeconds;
                DropRatio = (float)_protocol.PacketDropRatio;
                OtherDropRatio = (float)_protocol.OtherPacketDropRatio;
            }

            while (_updateActions.Count > 0)
            {
                _updateActions.Dequeue()();
            }
        }

        private void OnSocketError(object sender, System.IO.ErrorEventArgs e)
        {
            print("WebSocket Error!");

            if(e != null && e.GetException() != null)
            {
                print (e.GetException().ToString ());
            }

            if (ConnectionError != null)
            {
                ConnectionError(this, new EventArgs());
            }
        }

        private void OnSocketClose(object sender, EventArgs e)
        {
            bool wasConnected = IsConnected;
            IsConnected = false;

            if (wasConnected)
            {
                print("WebSocket Closed!");

                if (Disconnected != null)
                {
                    Disconnected(this, new EventArgs());
                }
            }

            _nextConnectAttemptTime = 5.0f;
        }

        private void OnSocketOpen(object sender, EventArgs eventArgs)
        {
            print("WebSocket Opened!");
            IsConnected = true;
            _nextConnectAttemptTime = float.MaxValue;

            Send(typeof(OriumRegistrationSerializer), Role == OriumRole.Audience);

            if (Connected != null)
            {
                Connected(this, new EventArgs());
            }
        }

        private void OnSeatAllocated(long timestamp, ulong channel, object packet)
        {
            _updateActions.Enqueue(delegate
                {
                    int seatIndex = (int)packet;

                    if (seatIndex != -1)
                    {
                        _allocatedSeat = seatIndex;

                        OriumUnitySeat seat = null;
                        if(!_seats.TryGetValue(seatIndex, out seat))
                        {
                            UnityEngine.Debug.LogError("Tried to be allocated to a seat that doesn't exist: " + seatIndex);
                            return;
                        }

                        GameObject cardboardMain = GameObject.Find("OriumUnityCardboardMain");

                        if(cardboardMain == null)
                            cardboardMain = GameObject.Find("OriumUnityCardboardMain(Clone)");

                        if(cardboardMain == null)
                        {
                            UnityEngine.Object cardboardMainPrefab = Resources.Load("OriumUnityCardboardMain");
                            cardboardMain = (GameObject) Instantiate(cardboardMainPrefab);
                        }

                        if(cardboardMain == null)
                        {
                            throw new Exception("Could not find or instantiate OriumUnityCardboardMain!");
                        }

                        cardboardMain.transform.SetParent(seat.transform, false);

                        OriumUnityTransformSender headSender = cardboardMain.GetComponentInChildren<OriumUnityTransformSender>();

                        if(headSender != null)
                        {
                            headSender.Channel = "Orium.AudienceHead." + seatIndex;
                        }

                        OriumUnityVoiceSender voiceSender = cardboardMain.GetComponentInChildren<OriumUnityVoiceSender>();

                        if(voiceSender != null)
                        {
                            voiceSender.Channel = "Orium.AudienceVoice." + seatIndex;
                        }

                        seat.SetSeatOccupation(true);

                        print("Ushered to seat " + seatIndex);
                    }
                });
        }

        private void OnSeatOccupied(long timestamp, ulong channel, object packet)
        {
            _updateActions.Enqueue(delegate
                {
                    OriumSeatOccupationPacket seatPacket = (OriumSeatOccupationPacket) packet;
                    OriumUnitySeat seat;
                    if(_seats.TryGetValue(seatPacket.SeatIndex, out seat))
                    {
                        seat.SetSeatOccupation(seatPacket.IsOccupied);
                    }
                });
        }

        private void OnMessageFallback(long timestamp, ulong hash, ulong channel, byte[] data)
        {
            ulong voiceChatHash = OriumUtils.CalculateHash("VoiceChatMsg");
            ulong allocationHash = OriumUtils.CalculateHash("Orium.SeatAllocation");
            ulong occupationHash = OriumUtils.CalculateHash("Orium.SeatOccupation");
            ulong registrationHash = OriumUtils.CalculateHash("Orium.Registration");
            ulong transformHash = OriumUtils.CalculateHash("TransformMsg");
            ulong audienceVoiceHash = OriumUtils.CalculateHash("AudienceVoice");
            ulong audienceVoice0Hash = OriumUtils.CalculateHash("Orium.AudienceVoice.0");
            ulong audienceVoice1Hash = OriumUtils.CalculateHash("Orium.AudienceVoice.1");

            if (_protocol.Registry.HasSerializer(hash))
            {
                UnityEngine.Debug.LogWarning("OriumRegistry failed to deserialize message!");
            }
            else
            {
                UnityEngine.Debug.LogWarning("Unrecognized message received");
            }
        }

    }

}
