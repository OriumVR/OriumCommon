﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Anima.Orium;
using Anima.Orium.Unity;

namespace Anima.Orium.Unity {

    public class OriumUnityTransformSender : MonoBehaviour {

        private OriumUnitySendChannel _sendChannel = null;
        [SerializeField]
        private string _channel = null;
        public string Channel
        {
            get { return _channel; }
            set {
                if (value != _channel)
                {
                    _channel = value;
                    _sendChannel = OriumUnityClient.Instance.CreateSendChannel(typeof(OriumTransformSerializer), _channel);
                }
            }
        }
        void OnEnable()
        {
            _sendChannel = OriumUnityClient.Instance.CreateSendChannel(typeof(OriumTransformSerializer), Channel);
        }
        void OnDisable()
        {
            _sendChannel = null;
        }

        public OriumRole Role = OriumRole.Broadcaster;

        // Update is called once per frame
        void Update () {
            if (_sendChannel == null)
            {
                return;
            }

            OriumTransformPacket packet = new OriumTransformPacket();
            packet.translation[0] = gameObject.transform.localPosition.x;
            packet.translation[1] = gameObject.transform.localPosition.y;
            packet.translation[2] = gameObject.transform.localPosition.z;

            packet.rotation[0] = gameObject.transform.localRotation.x;
            packet.rotation[1] = gameObject.transform.localRotation.y;
            packet.rotation[2] = gameObject.transform.localRotation.z;
            packet.rotation[3] = gameObject.transform.localRotation.w;

            packet.scale[0] = gameObject.transform.localScale.x;
            packet.scale[1] = gameObject.transform.localScale.y;
            packet.scale[2] = gameObject.transform.localScale.z;

            _sendChannel.Send(packet);
        }
    }

}
