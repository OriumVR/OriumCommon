﻿using UnityEngine;
using System.Collections;

public class OriumDownload : MonoBehaviour {

    #if UNITY_ANDROID && !UNITY_EDITOR
    private static string fullClassName = "com.Anima.Orium.OriumDownloadManager";
    #endif

    public static void CreateDownloadIntent(int id, string localPath, string remoteUri, bool immediate)
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
        if (pluginClass != null)
        {
            pluginClass.CallStatic("CreateDownloadIntent", id, localPath, remoteUri, immediate);
        }
        #endif
    }

    public static void CancelDownloadIntent(int id)
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaClass pluginClass = new AndroidJavaClass(fullClassName);
        if (pluginClass != null)
        {
        pluginClass.CallStatic("CancelDownloadIntent", id);
        }
        #endif
    }

    public void CreateDefaultDownloadIntent()
    {
        Debug.Log("Creating Default Download Intent");

        CreateDownloadIntent(0, "file://" + Application.persistentDataPath + "/testbundle", "http://www.orium-vr.com/files/testbundle", true);
    }
}
