﻿using UnityEngine;
using System.Collections;
using Anima.Orium.Unity;
using Anima.Orium;
using VoiceChat;
using System.IO;

namespace Anima.Orium.Unity {

    public class OriumUnityVoiceSender : MonoBehaviour {

        private OriumUnitySendChannel _sendChannel = null;
        [SerializeField]
        private string _channel = null;
        public string Channel
        {
            get { return _channel; }
            set {
                if (value != _channel)
                {
                    _channel = value;
                    _sendChannel = OriumUnityClient.Instance.CreateSendChannel(typeof(OriumVoiceSerializer), _channel);
                }
            }
        }
        void OnEnable()
        {
            _sendChannel = OriumUnityClient.Instance.CreateSendChannel(typeof(OriumVoiceSerializer), Channel);
        }
        void OnDisable()
        {
            _sendChannel = null;
        }

        private bool _justConnected = false;
        private bool _justDisconnected = false;

    	// Use this for initialization
    	void Start () {
            OriumUnityClient client = OriumUnityClient.Instance;
            
            if (client != null)
            {
                // TODO: Remove these event handlers on destruction
                client.Connected += (o, e) => 
                {
                    _justConnected = true;
                };
                client.Disconnected += (o, e) =>
                {
                    _justDisconnected = true;
                };
            }
        }

        void Update() {
            VoiceChatRecorder voiceRecorder = VoiceChatRecorder.Instance;

            if (_justConnected)
            {
                if (voiceRecorder != null && voiceRecorder.AvailableDevices.Length > 0) {
                    voiceRecorder.NewSample += OnNewVoiceSample;

//                    voiceRecorder.Device = voiceRecorder.AvailableDevices[0];
                    voiceRecorder.StartRecording ();
                }

                _justConnected = false;
            }

            if (_justDisconnected)
            {
                if (voiceRecorder != null)
                {
                    voiceRecorder.StopRecording();
                    voiceRecorder.NewSample -= OnNewVoiceSample;
                }

                _justDisconnected = false;
            }
        }
    	
        void OnNewVoiceSample (VoiceChatPacket packet)
        {
            if (_sendChannel != null)
            {
                _sendChannel.Send(packet);
            }
        }
    }

}
